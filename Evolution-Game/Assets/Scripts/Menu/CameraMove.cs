﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraMove : MonoBehaviour {

    public Animation animation;
    public bool switchScene = false;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if (switchScene)
        {
            SceneManager.LoadScene("game");
        }
    }

    public void exit ()
    {
        Application.Quit();
    } 

    public void go()
    {
        animation.Play();
    }

}
